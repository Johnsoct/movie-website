import fresh_tomatoes
import media

# API configuration relevant to building image URLs
CONFIG_URL = 'https://api.themoviedb.org/3/configuration?api_key=215fee8539b77f6368fbbcd9a197bb2e'

# Format: name of movie : movie url w/ movie ID
movie_dict = {
    'jason_bourne': {
        'themoviedb_url':'https://api.themoviedb.org/3/movie/324668?api_key=215fee8539b77f6368fbbcd9a197bb2e',

        'youtube_trailer': 'https://youtu.be/F4gJsKZvqE4'
    },
    'godfather_the': {
        'themoviedb_url':'https://api.themoviedb.org/3/movie/tt0068646?api_key=215fee8539b77f6368fbbcd9a197bb2e',

        'youtube_trailer': 'https://youtu.be/dNE2PvbesQU'
    },
    'avatar': {
        'themoviedb_url':'https://api.themoviedb.org/3/movie/tt0499549?api_key=215fee8539b77f6368fbbcd9a197bb2e',

        'youtube_trailer':'https://youtu.be/5PSNL1qE6VY'
    },
    'deadpool': {
        'themoviedb_url':'https://api.themoviedb.org/3/movie/tt1431045?api_key=215fee8539b77f6368fbbcd9a197bb2e',

        'youtube_trailer':'https://youtu.be/Xithigfg7dA'
    }
}

# Creating class instances - CONFIG_URL is always the first parameter passed
jason_bourne = media.Movie(CONFIG_URL,
        movie_dict['jason_bourne']['themoviedb_url'],
        movie_dict['jason_bourne']['youtube_trailer'])
godfather_the = media.Movie(CONFIG_URL,
        movie_dict['godfather_the']['themoviedb_url'],
        movie_dict['godfather_the']['youtube_trailer'])
avatar = media.Movie(CONFIG_URL,
        movie_dict['avatar']['themoviedb_url'],
        movie_dict['avatar']['youtube_trailer'])
deadpool = media.Movie(CONFIG_URL,
        movie_dict['deadpool']['themoviedb_url'],
        movie_dict['deadpool']['youtube_trailer'])

movies_list = [jason_bourne, godfather_the, avatar, deadpool]

fresh_tomatoes.open_movies_page(movies_list)
