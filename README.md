# Favorite Movies
This program runs an example site of my favorite movies.

### Languages, Libraries, Frameworks
* python
* bootstrap
* jquery

### Installation
Download or clone from Gitlab: git@gitlab.com:Johnsoct/movie-website.git

### Code format and styling (python)
```
def image_url(self, config_url):
      """
      This function builds the image url required to get an image from a
      movie's poster_path
      """
      # call request function defined above to process APIs config for images
      result = self.request(config_url)
      base_url = result['images']['secure_base_url']
      size = result['images']['poster_sizes'][4]

      url = base_url + size
      return url
```

### License
Licensed under the terms of the MIT license.
