import requests
import json

class Movie():
    """ Base for Movie instances

        Attributes:
            title: movie title,
            runtime: duration of movie,
            box art: DVD box art,
            poster image: image of the movie advertisement,
            trailer URLS: youtube link to movie trailer


        APIkey = 215fee8539b77f6368fbbcd9a197bb2e
    """

    def __init__(self, config_url, themoviedb_url, trailer_url):
        """
        Inits an instance of Movie using the results of the json data from
        get_movie_from_themoviedb
        """
        # result stores the json response
        result = self.request(themoviedb_url)
        # image_url stores the pre-built image url for later customization
        image_url = self.image_url(config_url)
        # result[''] is accessing the json response of result
        self.title = result['title']
        self.runtime = result['runtime']
        self.description = result['overview']
        # adds instance's poster_path to the pre-built image url
        self.poster_image_url = (image_url + result['poster_path'])
        self.trailer_youtube_url = trailer_url

    def request(self, themoviedb_url):
        """
        Reads a url query string from themoviedb and returns data (json)

        If a success status code is not returned, throw error; otherwise, on
        success, return json response.
        """
        # requests is a python library for http requests
        resp = requests.get(themoviedb_url)
        if resp.status_code != 200:
            # something went wrong
            raise ApiError('Get /movie/{}'.format(resp.status_code))
        # store resp in json format
        result = resp.json()
        return result;

    def image_url(self, config_url):
        """
        This function builds the image url required to get an image from a
        movie's poster_path
        """
        # call request function defined above to process APIs config for images
        result = self.request(config_url)
        base_url = result['images']['secure_base_url']
        size = result['images']['poster_sizes'][4]

        url = base_url + size
        return url
